### What is this repository for? ###
This repository contains a modified .ycm_extra_conf.py for YouCompleteMe. Everytime flags are being parsed for a file, it tries to parse the project-specific flags from CMake. It assumes that the CMake-Files are found in subfolders of its location.

### How do I get set up? ###

Place the .ycm_extra_conf.py in the root of your project, e.g.: /home/user/code/src/my_awesome_project/.ycm_extra_conf.py

Everything should work out of the box now.
